package com.example.citydistance.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.citydistance.dto.EdgeMetaDTO;
import com.example.citydistance.service.DistanceService;
import com.example.citydistance.service.NoSuchEntityException;
import com.example.citydistance.service.NodeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DistanceServiceImplTest {
    @Autowired
    private DistanceService distanceService;

    @Autowired
    private NodeService nodeService;

    @Test
    public void getByIds_twoCities_ok() throws NoSuchEntityException {
        // Arrange
        var edgeMeta = new EdgeMetaDTO(100D);
        var edges = nodeService.connectByNames("Istanbul", "Kinshasa", edgeMeta);
        var ids = edges.stream().map(n -> n.getNodeFromId()).sorted().toArray(Long[]::new);

        // Act
        var distances = distanceService.getByNodeIds(ids[0], ids[1]);
        var distance = distances.iterator().next();
        var path = distance.getPath();

        // Assert
        assertFalse(distances.isEmpty());
        assertEquals(path.size(), 2);
        assertEquals(path.get(0), "Istanbul");
        assertEquals(path.get(1), "Kinshasa");
        assertEquals(distance.getDistanceTotal(), edgeMeta.getDistance());
    }

    @Test
    public void getByIds_threeCities_ok() throws NoSuchEntityException {
        // Arrange
        var firstEdgeMeta = new EdgeMetaDTO(100D);
        var secondEdgeMeta = new EdgeMetaDTO(200D);
        var firstEdges = nodeService.connectByNames("Bhopal", "İzmir", firstEdgeMeta);
        var secondEdges = nodeService.connectByNames("İzmir", "Chengdu", secondEdgeMeta);
        var firstIds = firstEdges.stream().map(n -> n.getNodeFromId()).sorted().toArray(Long[]::new);
        var secondIds = secondEdges.stream().map(n -> n.getNodeFromId()).sorted().toArray(Long[]::new);
        var expectedTotalDistance = firstEdgeMeta.getDistance() + secondEdgeMeta.getDistance();

        // Act
        var distances = distanceService.getByNodeIds(firstIds[0], secondIds[1]);
        var distance = distances.iterator().next();
        var path = distance.getPath();

        // Assert
        assertFalse(distances.isEmpty());
        assertEquals(path.size(), 3);
        assertEquals(path.get(0), "Bhopal");
        assertEquals(path.get(1), "İzmir");
        assertEquals(path.get(2), "Chengdu");
        assertEquals(distance.getDistanceTotal(), expectedTotalDistance, 0.001);
    }

    @Test(expected = NoSuchEntityException.class)
    public void getByIds_twoCities_error() throws NoSuchEntityException {
        // Arrange & Act & Assert
        distanceService.getByNodeIds(1000L, 2000L);
    }

    @Test
    public void getByNames_twoCities_ok() throws NoSuchEntityException {
        // Arrange
        var edgeMeta = new EdgeMetaDTO(100D);
        nodeService.connectByNames("Abu Dhabi", "Busan", edgeMeta);

        // Act
        var distances = distanceService.getByNodeNames("Abu Dhabi", "Busan");
        var distance = distances.iterator().next();
        var path = distance.getPath();

        // Assert
        assertFalse(distances.isEmpty());
        assertEquals(path.size(), 2);
        assertEquals(path.get(0), "Abu Dhabi");
        assertEquals(path.get(1), "Busan");
        assertEquals(distance.getDistanceTotal(), edgeMeta.getDistance());
    }

    @Test
    public void getByNames_threeCities_ok() throws NoSuchEntityException {
        // Arrange
        var firstEdgeMeta = new EdgeMetaDTO(300D);
        var secondEdgeMeta = new EdgeMetaDTO(400D);
        nodeService.connectByNames("Kathmandu", "T'bilisi", firstEdgeMeta);
        nodeService.connectByNames("T'bilisi", "Dakar", secondEdgeMeta);
        var expectedTotalDistance = firstEdgeMeta.getDistance() + secondEdgeMeta.getDistance();

        // Act
        var distances = distanceService.getByNodeNames("Kathmandu", "Dakar");
        var distance = distances.iterator().next();
        var path = distance.getPath();

        // Assert
        assertFalse(distances.isEmpty());
        assertEquals(path.size(), 3);
        assertEquals(path.get(0), "Kathmandu");
        assertEquals(path.get(1), "T'bilisi");
        assertEquals(path.get(2), "Dakar");
        assertEquals(distance.getDistanceTotal(), expectedTotalDistance, 0.001);
    }

    @Test(expected = NoSuchEntityException.class)
    public void getByNames_twoCities_error() throws NoSuchEntityException {
        // Arrange & Act & Assert
        distanceService.getByNodeNames("Karaj", "Calgary");
    }
}
