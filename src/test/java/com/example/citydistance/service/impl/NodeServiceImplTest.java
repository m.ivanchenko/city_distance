package com.example.citydistance.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.citydistance.dto.EdgeMetaDTO;
import com.example.citydistance.dto.NodeDTO;
import com.example.citydistance.service.NoSuchEntityException;
import com.example.citydistance.service.NodeService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NodeServiceImplTest {
    @Autowired
    private NodeService nodeService;

    @Test
    public void create_ok() {
        // Arrange & Act
        var nodes = nodeService.create(Collections.singleton(new NodeDTO(null, "Mendoza")));

        // Assert
        assertFalse(nodes.isEmpty());
    }

    @Test
    public void getById_ok() {
        // Arrange
        var newNodes = nodeService.create(Collections.singleton(new NodeDTO(null, "Tampere")));

        // Act
        var node = nodeService.getById(newNodes.iterator().next().getId());

        // Assert
        assertTrue(node.isPresent());
    }

    @Test
    public void getByName_ok() {
        // Arrange
        nodeService.create(Collections.singleton(new NodeDTO(null, "Tampere")));

        // Act
        var node = nodeService.getByName("Tampere");

        // Assert
        assertTrue(node.isPresent());
    }

    @Test
    public void connectByIds_ok() throws NoSuchEntityException {
        // Arrange
        var models = Set.of(new NodeDTO(null, "Beijing"), new NodeDTO(null, "Hanoi"));
        var newNodes = nodeService.create(models);
        var ids = newNodes.stream().map(n -> n.getId()).sorted().toArray(Long[]::new);

        // Act
        var connections = nodeService.connectByIds(ids[0], ids[1], new EdgeMetaDTO(100D));

        // Assert
        assertFalse(connections.isEmpty());
    }

    @Test
    public void connectByNames_bothExist_ok() throws NoSuchEntityException {
        // Arrange
        var models = Set.of(new NodeDTO(null, "Semarang"), new NodeDTO(null, "Nagpur"));
        var newNodes = nodeService.create(models);
        var names = newNodes.stream().map(n -> n.getName()).toArray(String[]::new);

        // Act
        var connections = nodeService.connectByNames(names[0], names[1], new EdgeMetaDTO(200D));

        // Assert
        assertFalse(connections.isEmpty());
    }

    @Test
    public void connectByNames_bothNonexistent_ok() throws NoSuchEntityException {
        // Arrange & Act
        var connections = nodeService.connectByNames("Mexico City", "Yokohama", new EdgeMetaDTO(300D));

        // Assert
        assertFalse(connections.isEmpty());
    }

    @Test
    public void findPathByIds_bothExist_ok() throws NoSuchEntityException {
        // Arrange
        var models = Set.of(new NodeDTO(null, "Casablanca"), new NodeDTO(null, "Peshawar"), new NodeDTO(null, "Cali"));
        var newNodes = nodeService.create(models);
        var ids = newNodes.stream().map(n -> n.getId()).sorted().toArray(Long[]::new);
        nodeService.connectByIds(ids[0], ids[1], new EdgeMetaDTO(400D));
        nodeService.connectByIds(ids[1], ids[2], new EdgeMetaDTO(500D));

        // Act
        var paths = nodeService.getPathByIds(ids[0], ids[2]);
        var path = paths.iterator().next();

        // Assert
        assertFalse(paths.isEmpty());
        assertEquals(path.size(), 3);
        assertEquals(path.get(0), ids[0]);
        assertEquals(path.get(1), ids[1]);
        assertEquals(path.get(2), ids[2]);
    }

    @Test(expected = NoSuchEntityException.class)
    public void findPathByIds_bothNonexistent_error() throws NoSuchEntityException {
        // Arrange & Act & Assert
        nodeService.getPathByIds(1000L, 2000L);
    }
}
