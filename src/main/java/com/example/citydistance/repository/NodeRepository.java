package com.example.citydistance.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.citydistance.domain.Node;

public interface NodeRepository extends JpaRepository<Node, Long> {
    Optional<Node> findByNameIgnoreCase(String name);
}
