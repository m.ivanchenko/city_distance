package com.example.citydistance.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.citydistance.domain.Edge;

public interface EdgeRepository extends JpaRepository<Edge, Long> {
    Optional<Edge> findOneByNodeFromIdAndNodeToId(Long nodeFromId, Long nodeToId);
}
