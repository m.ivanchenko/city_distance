package com.example.citydistance.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
public class Node implements Serializable {
    public final static int MIN_NAME_LENGTH = 1;
    public final static int MAX_NAME_LENGTH = 100;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Length(min = MIN_NAME_LENGTH, max = Node.MAX_NAME_LENGTH)
    private String name;

    @OneToMany(mappedBy = "nodeFrom", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Edge> edges = new HashSet<Edge>();

    @Version
    private Long version;

    public Node() {
    }

    public Node(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public void setEdges(Set<Edge> edges) {
        this.edges = edges;
    }
}
