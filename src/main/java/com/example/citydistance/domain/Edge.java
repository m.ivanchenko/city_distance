package com.example.citydistance.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
public class Edge implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    private Node nodeFrom;

    @NotNull
    @ManyToOne
    private Node nodeTo;

    @OneToOne(mappedBy = "edge", cascade = CascadeType.ALL)
    private EdgeMeta edgeMeta;

    @Version
    private Long version;

    public Edge() {
    }

    public Edge(Node nodeFrom, Node nodeTo) {
        this.nodeFrom = nodeFrom;
        this.nodeTo = nodeTo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Node getNodeFrom() {
        return nodeFrom;
    }

    public void setNodeFrom(Node nodeFrom) {
        this.nodeFrom = nodeFrom;
    }

    public Node getNodeTo() {
        return nodeTo;
    }

    public void setNodeTo(Node nodeTo) {
        this.nodeTo = nodeTo;
    }

    public EdgeMeta getEdgeMeta() {
        return edgeMeta;
    }

    private void setEdgeMeta(EdgeMeta edgeMeta) {
        this.edgeMeta = edgeMeta;
    }

    public void saveEdgeMeta(EdgeMeta edgeMeta) throws NullPointerException {
        if (edgeMeta == null) {
            throw new NullPointerException("Edge meta must not be null");
        }

        if (this.edgeMeta != null) {
            this.edgeMeta.setDistance(edgeMeta.getDistance());
        } else {
            setEdgeMeta(edgeMeta);
            edgeMeta.setEdge(this);
        }
    }
}
