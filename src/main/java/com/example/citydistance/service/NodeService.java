package com.example.citydistance.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.citydistance.dto.EdgeDTO;
import com.example.citydistance.dto.EdgeMetaDTO;
import com.example.citydistance.dto.NodeDTO;

public interface NodeService {
    Set<NodeDTO> create(Set<NodeDTO> nodes);

    Optional<NodeDTO> getById(Long id);

    Optional<NodeDTO> getByName(String name);

    Set<EdgeDTO> connectByIds(Long fromId, Long toId, EdgeMetaDTO edgeMeta) throws NoSuchEntityException;

    Set<EdgeDTO> connectByNames(String fromName, String toName, EdgeMetaDTO edgeMeta) throws NoSuchEntityException;

    Set<List<Long>> getPathByIds(Long fromId, Long toId) throws NoSuchEntityException;
}
