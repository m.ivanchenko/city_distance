package com.example.citydistance.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.citydistance.dto.DistanceDTO;
import com.example.citydistance.service.DistanceService;
import com.example.citydistance.service.EdgeService;
import com.example.citydistance.service.NoSuchEntityException;
import com.example.citydistance.service.NodeService;

@Service
@Transactional
public class DistanceServiceImpl implements DistanceService {
    private NodeService nodeService;

    private EdgeService edgeService;

    @Autowired
    public DistanceServiceImpl(NodeService nodeService, EdgeService edgeService) {
        this.nodeService = nodeService;
        this.edgeService = edgeService;
    }

    @Override
    public Set<DistanceDTO> getByNodeIds(Long nodeFromId, Long nodeToId) throws NoSuchEntityException {
        var paths = nodeService.getPathByIds(nodeFromId, nodeToId);

        var distances = new HashSet<DistanceDTO>();
        for (var path : paths) {
            var names = new ArrayList<String>();
            var distanceTotal = 0D;

            for (int i = 1; i < path.size(); i++) {
                var pairFromId = path.get(i - 1);
                var pairToId = path.get(i);

                var edge = edgeService.getByNodeIds(pairFromId, pairToId);
                if (edge.isPresent()) {
                    distanceTotal += edge.get().getEdgeMeta().getDistance();
                }
            }

            for (var nodeId : path) {
                var node = nodeService.getById(nodeId);

                if (node.isPresent()) {
                    names.add(node.get().getName());
                }
            }

            distances.add(new DistanceDTO(names, distanceTotal));
        }

        return distances;
    }

    @Override
    public Set<DistanceDTO> getByNodeNames(String nodeFromName, String nodeToName) throws NoSuchEntityException {
        var nodeFrom = nodeService.getByName(nodeFromName);
        var nodeTo = nodeService.getByName(nodeToName);

        if (!nodeFrom.isPresent()) {
            throw new NoSuchEntityException(String.format("City '%s' was not found", nodeFromName));
        }

        if (!nodeTo.isPresent()) {
            throw new NoSuchEntityException(String.format("City '%s' was not found", nodeToName));
        }

        return getByNodeIds(nodeFrom.get().getId(), nodeTo.get().getId());
    }
}
