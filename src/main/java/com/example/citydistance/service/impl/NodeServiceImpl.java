package com.example.citydistance.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.citydistance.domain.Edge;
import com.example.citydistance.domain.EdgeMeta;
import com.example.citydistance.domain.Node;
import com.example.citydistance.dto.EdgeDTO;
import com.example.citydistance.dto.EdgeMetaDTO;
import com.example.citydistance.dto.NodeDTO;
import com.example.citydistance.repository.EdgeRepository;
import com.example.citydistance.repository.NodeRepository;
import com.example.citydistance.service.NoSuchEntityException;
import com.example.citydistance.service.NodeService;

@Service
@Transactional
public class NodeServiceImpl implements NodeService {
    private NodeRepository nodeRepository;

    private EdgeRepository edgeRepository;

    @Autowired
    public NodeServiceImpl(NodeRepository nodeRepository, EdgeRepository edgeRepository) {
        this.nodeRepository = nodeRepository;
        this.edgeRepository = edgeRepository;
    }

    private Optional<NodeDTO> mapToDTO(Optional<Node> entity) {
        if (entity.isPresent()) {
            var node = entity.get();
            var nodeDTO = new NodeDTO(node.getId(), node.getName());
            return Optional.of(nodeDTO);
        }

        return Optional.empty();
    }

    private Edge connect(Node nodeFrom, Node nodeTo) {
        var existingEdge = nodeFrom.getEdges().stream().filter(e -> e.getNodeTo().getId().equals(nodeTo.getId()))
                .findFirst();
        if (existingEdge.isPresent()) {
            return existingEdge.get();
        }

        var edge = new Edge(nodeFrom, nodeTo);
        nodeFrom.getEdges().add(edge);

        return edge;
    }

    private Set<List<Long>> depthFirstSearch(Node from, Node to, Set<Long> path) {
        path.add(from.getId());

        if (from.getId().equals(to.getId())) {
            var paths = new HashSet<List<Long>>();
            paths.add(new ArrayList<Long>(Collections.singletonList(from.getId())));
            return paths;
        }

        var paths = new HashSet<List<Long>>();
        for (var e : from.getEdges()) {
            var next = e.getNodeTo();
            if (!path.contains(next.getId())) {
                var search = depthFirstSearch(next, to, new HashSet<Long>(path));
                for (var p : search) {
                    p.add(0, from.getId());
                    paths.add(p);
                }
            }
        }

        return paths;
    }

    private Set<List<Long>> path(Node from, Node to) {
        return depthFirstSearch(from, to, new HashSet<Long>());
    }

    @Override
    public Set<NodeDTO> create(Set<NodeDTO> nodes) {
        var newNodes = nodeRepository
                .saveAll(nodes.stream().map(n -> new Node(n.getName())).collect(Collectors.toList()));

        return newNodes.stream().map(n -> new NodeDTO(n.getId(), n.getName())).collect(Collectors.toSet());
    }

    @Override
    public Optional<NodeDTO> getById(Long id) {
        return mapToDTO(nodeRepository.findById(id));
    }

    @Override
    public Optional<NodeDTO> getByName(String name) {
        return mapToDTO(nodeRepository.findByNameIgnoreCase(name));
    }

    @Override
    public Set<EdgeDTO> connectByIds(Long fromId, Long toId, EdgeMetaDTO edgeMeta) throws NoSuchEntityException {
        var nodeFrom = nodeRepository.findById(fromId).orElse(null);
        var nodeTo = nodeRepository.findById(toId).orElse(null);

        if (nodeFrom == null || nodeTo == null) {
            throw new NoSuchEntityException("Both endpoints must exist");
        }

        var edges = Arrays.asList(connect(nodeFrom, nodeTo), connect(nodeTo, nodeFrom));
        for (var e : edges) {
            e.saveEdgeMeta(new EdgeMeta(edgeMeta.getDistance()));
        }

        edges = edgeRepository.saveAll(edges);

        var results = new HashSet<EdgeDTO>();
        for (var e : edges) {
            results.add(new EdgeDTO(e.getId(), e.getNodeFrom().getId(), e.getNodeTo().getId(),
                    new EdgeMetaDTO(e.getEdgeMeta().getDistance())));
        }

        return results;
    }

    @Override
    public Set<EdgeDTO> connectByNames(String fromName, String toName, EdgeMetaDTO edgeMeta)
            throws NoSuchEntityException {
        var nodeFrom = nodeRepository.findByNameIgnoreCase(fromName).orElse(null);
        var nodeTo = nodeRepository.findByNameIgnoreCase(toName).orElse(null);

        if (nodeFrom == null) {
            nodeFrom = nodeRepository.save(new Node(fromName));
        }

        if (nodeTo == null) {
            nodeTo = nodeRepository.save(new Node(toName));
        }

        return connectByIds(nodeFrom.getId(), nodeTo.getId(), edgeMeta);
    }

    @Override
    public Set<List<Long>> getPathByIds(Long fromId, Long toId) throws NoSuchEntityException {
        var nodeFrom = nodeRepository.findById(fromId);
        var nodeTo = nodeRepository.findById(toId);

        if (!nodeFrom.isPresent() || !nodeTo.isPresent()) {
            throw new NoSuchEntityException("Both endpoints must be set");
        }

        return path(nodeFrom.get(), nodeTo.get());
    }
}
