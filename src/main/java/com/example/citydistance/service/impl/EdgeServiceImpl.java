package com.example.citydistance.service.impl;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.citydistance.domain.Edge;
import com.example.citydistance.dto.EdgeDTO;
import com.example.citydistance.dto.EdgeMetaDTO;
import com.example.citydistance.dto.EdgeWithNameDTO;
import com.example.citydistance.repository.EdgeRepository;
import com.example.citydistance.service.EdgeService;
import com.example.citydistance.service.NoSuchEntityException;
import com.example.citydistance.service.NodeService;

@Service
@Transactional
public class EdgeServiceImpl implements EdgeService {
    private EdgeRepository edgeRepository;

    private NodeService nodeService;

    @Autowired
    public EdgeServiceImpl(EdgeRepository edgeRepository, NodeService nodeService) {
        this.edgeRepository = edgeRepository;
        this.nodeService = nodeService;
    }

    private Optional<EdgeDTO> mapToDTO(Optional<Edge> entity) {
        if (entity.isPresent()) {
            var edge = entity.get();
            var edgeMeta = new EdgeMetaDTO(edge.getEdgeMeta().getDistance());
            var edgeDTO = new EdgeDTO(edge.getId(), edge.getNodeFrom().getId(), edge.getNodeTo().getId(), edgeMeta);

            return Optional.of(edgeDTO);
        }

        return Optional.empty();
    }

    @Override
    public Set<EdgeDTO> createByNodeIds(Set<EdgeDTO> edges) throws NoSuchEntityException {
        var results = new HashSet<EdgeDTO>();

        for (var e : edges) {
            var connections = nodeService.connectByIds(e.getNodeFromId(), e.getNodeToId(), e.getEdgeMeta());
            results.addAll(connections);
        }

        return results;
    }

    @Override
    public Set<EdgeDTO> createByNodeNames(Set<EdgeWithNameDTO> edges) throws NoSuchEntityException {
        var results = new HashSet<EdgeDTO>();
        for (var e : edges) {
            var connections = nodeService.connectByNames(e.getNodeFromName(), e.getNodeToName(), e.getEdgeMeta());
            results.addAll(connections);
        }

        return results;
    }

    @Override
    public Optional<EdgeDTO> getById(Long id) {
        return mapToDTO(edgeRepository.findById(id));
    }

    @Override
    public Optional<EdgeDTO> getByNodeIds(Long nodeFromId, Long nodeToId) {
        return mapToDTO(edgeRepository.findOneByNodeFromIdAndNodeToId(nodeFromId, nodeToId));
    }
}
