package com.example.citydistance.service;

import java.util.Optional;
import java.util.Set;

import com.example.citydistance.dto.EdgeDTO;
import com.example.citydistance.dto.EdgeWithNameDTO;

public interface EdgeService {
    Set<EdgeDTO> createByNodeIds(Set<EdgeDTO> edges) throws NoSuchEntityException;

    Set<EdgeDTO> createByNodeNames(Set<EdgeWithNameDTO> edges) throws NoSuchEntityException;

    Optional<EdgeDTO> getById(Long id);

    Optional<EdgeDTO> getByNodeIds(Long nodeFromId, Long nodeToId);
}
