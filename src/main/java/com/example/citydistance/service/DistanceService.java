package com.example.citydistance.service;

import java.util.Set;

import com.example.citydistance.dto.DistanceDTO;

public interface DistanceService {
    Set<DistanceDTO> getByNodeIds(Long nodeFromId, Long nodeToId) throws NoSuchEntityException;

    Set<DistanceDTO> getByNodeNames(String nodeFromName, String nodeToName) throws NoSuchEntityException;
}
