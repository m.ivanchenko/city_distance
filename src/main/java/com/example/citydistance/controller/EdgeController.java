package com.example.citydistance.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.citydistance.dto.EdgeDTO;
import com.example.citydistance.dto.EdgeWithNameDTO;
import com.example.citydistance.service.EdgeService;
import com.example.citydistance.service.NoSuchEntityException;

@RestController
@RequestMapping("/api/edge")
public class EdgeController {
    private EdgeService edgeService;

    @Autowired
    public EdgeController(EdgeService edgeService) {
        this.edgeService = edgeService;
    }

    @PostMapping
    public ResponseEntity<?> createByNodeNames(@RequestBody Set<EdgeWithNameDTO> edges) {
        try {
            var conections = edgeService.createByNodeNames(edges);

            return ResponseEntity.status(HttpStatus.CREATED).body(conections);
        } catch (NoSuchEntityException nsee) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(nsee.getMessage());
        }
    }

    @PostMapping("/byId")
    public ResponseEntity<?> createByNodeIds(@RequestBody Set<EdgeDTO> edges) {
        try {
            var conections = edgeService.createByNodeIds(edges);

            return ResponseEntity.status(HttpStatus.CREATED).body(conections);
        } catch (NoSuchEntityException nsee) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(nsee.getMessage());
        }
    }
}
