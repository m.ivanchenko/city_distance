package com.example.citydistance.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.citydistance.dto.DistanceDTO;
import com.example.citydistance.service.DistanceService;
import com.example.citydistance.service.NoSuchEntityException;

@RestController
@RequestMapping("/api/distance")
public class DistanceController {
    private DistanceService distanceService;

    public DistanceController(DistanceService distanceService) {
        this.distanceService = distanceService;
    }

    @GetMapping
    public ResponseEntity<?> getByNodeNames(@RequestParam String from, @RequestParam String to) {
        try {
            var distances = distanceService.getByNodeNames(from, to);

            if (distances.size() == 0) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .body(String.format("Path between '%s' and '%s' was not found", from, to));
            }

            return ResponseEntity.ok(distances);
        } catch (NoSuchEntityException nsee) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new DistanceDTO(nsee.getMessage()));
        }
    }

    @GetMapping("/byId")
    public ResponseEntity<?> getByNodeIds(@RequestParam Long from, @RequestParam Long to) {
        try {
            var distances = distanceService.getByNodeIds(from, to);

            if (distances == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }

            return ResponseEntity.ok(distances);
        } catch (NoSuchEntityException nsee) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new DistanceDTO(nsee.getMessage()));
        }
    }
}
