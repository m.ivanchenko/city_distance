package com.example.citydistance.dto;

import java.util.List;

public class DistanceDTO {
    private List<String> path;

    private Double distanceTotal;

    private String message;

    public DistanceDTO(String message) {
        this.message = message;
    }

    public DistanceDTO(List<String> path, Double distanceTotal) {
        this.path = path;
        this.distanceTotal = distanceTotal;
    }

    public List<String> getPath() {
        return path;
    }

    public Double getDistanceTotal() {
        return distanceTotal;
    }

    public String getMessage() {
        return message;
    }
}
