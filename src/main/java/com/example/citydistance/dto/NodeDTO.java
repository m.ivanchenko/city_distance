package com.example.citydistance.dto;

public class NodeDTO {
    private Long id;

    private String name;

    public NodeDTO() {
    }

    public NodeDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
