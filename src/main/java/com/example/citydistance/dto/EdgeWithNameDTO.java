package com.example.citydistance.dto;

public class EdgeWithNameDTO {
    private Long id;

    private String nodeFromName;

    private String nodeToName;

    private EdgeMetaDTO edgeMeta;

    public EdgeWithNameDTO() {
    }

    public EdgeWithNameDTO(Long id, String nodeFromName, String nodeToName, EdgeMetaDTO edgeMeta) {
        this.id = id;
        this.nodeFromName = nodeFromName;
        this.nodeToName = nodeToName;
        this.edgeMeta = edgeMeta;
    }

    public Long getId() {
        return id;
    }

    public String getNodeFromName() {
        return nodeFromName;
    }

    public String getNodeToName() {
        return nodeToName;
    }

    public EdgeMetaDTO getEdgeMeta() {
        return edgeMeta;
    }
}
