package com.example.citydistance.dto;

public class EdgeMetaDTO {
    private Double distance;

    public EdgeMetaDTO() {
    }

    public EdgeMetaDTO(Double distance) {
        this.distance = distance;
    }

    public Double getDistance() {
        return distance;
    }
}
