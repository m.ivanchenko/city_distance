package com.example.citydistance.dto;

public class EdgeDTO {
    private Long id;

    private Long nodeFromId;

    private Long nodeToId;

    private EdgeMetaDTO edgeMeta;

    public EdgeDTO() {
    }

    public EdgeDTO(Long id, Long nodeFromId, Long nodeToId, EdgeMetaDTO edgeMeta) {
        this.id = id;
        this.nodeFromId = nodeFromId;
        this.nodeToId = nodeToId;
        this.edgeMeta = edgeMeta;
    }

    public Long getId() {
        return id;
    }

    public Long getNodeFromId() {
        return nodeFromId;
    }

    public Long getNodeToId() {
        return nodeToId;
    }

    public EdgeMetaDTO getEdgeMeta() {
        return edgeMeta;
    }
}
