INSERT INTO node (id, name, version)
VALUES
	(1,'Cairo', 0),
	(2,'Athens', 0),
	(3,'Algiers', 0),
	(4,'Seattle', 0),
	(5,'Munich', 0),
	(6,'Singapore', 0),
	(7,'Dubai', 0),
	(8,'Varna', 0),
	(9,'Budapest', 0),
	(10,'Amsterdam', 0);

INSERT INTO edge (id, node_from_id, node_to_id, version)
VALUES
	(1, 1, 2, 0),
	(2, 2, 1, 0),
	(3, 1, 3, 0),
	(4, 3, 1, 0),
	(5, 2, 3, 0),
	(6, 3, 2, 0),
	(7, 3, 4, 0),
	(8, 4, 3, 0);

INSERT INTO edge_meta (edge_id, distance, version)
VALUES
	(1, 100, 0),
	(2, 100, 0),
	(3, 200, 0),
	(4, 200, 0),
	(5, 300, 0),
	(6, 300, 0),
	(7, 400, 0),
	(8, 400, 0);