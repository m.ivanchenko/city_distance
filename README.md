# City Distance Service
This service allows consumers to set and retrieve distance and path between two arbitrary cities.  
It exposes a total of five endpoints, three for resource creation and two for distance calculation:
1. [`/api/node`](README.md#apinode)
2. [`/api/edge`](README.md#apiedge)
3. [`/api/edge/byId`](README.md#apiedgebyId)
4. [`/api/distance`](README.md#apidistance)
5. [`/api/distance/byId`](README.md#apidistancebyId)

### **/api/node**
Accepts `HTTP POST` requests.
Request body should contain non-empty array of unique `NodeDTO` models.  
Creates city records.  
It will return HTTP status code `400 Bad Request`, if the creation failed and `201 Created` otherwise.  
Example in `JSON` format:
```json
[
  {
    "name": "Bogotá"
  },
  {
    "name": "Osaka"
  },
  {
    "name": "Braga"
  }
]
```

### **/api/edge**
Accepts `HTTP POST` requests.  
Request body should contain non-empty array of unique `EdgeWithNameDTO` models.  
Creates connections between any number of city pairs.  
If any of the city-nodes are not present in the database, they will be created automatically.  
It will return HTTP status code `400 Bad Request`, if the creation failed and `201 Created` otherwise.  
Example in `JSON` format:
```json
[
  {
    "nodeFromName": "Kyiv",
    "nodeToName": "Minsk",
    "edgeMeta": {
    "distance": 582.8
    }
  },
  {
    "nodeFromName": "Athens",
    "nodeToName": "Munich",
    "edgeMeta": {
    "distance": 2029.9
    }
  }
]
```

### **/api/edge/byId**
Accepts `HTTP POST` requests.
Request body should contain non-empty array of unique `EdgeDTO` models.  
Creates connections between any number of city pairs, where each city is already present in the database, querying them by id.  
It will return HTTP status code `400 Bad Request`, if any of the requested cities does not exist in the database and `201 Created` otherwise.  
Example in `JSON` format:
```json
[
  {
    "nodeFromId": 1,
    "nodeToId": 2,
    "edgeMeta": {
      "distance": 843.3
    }
  },
  {
    "nodeFromId": 63,
    "nodeToId": 31,
    "edgeMeta": {
      "distance": 190.0
    }
  }
]
```

### **/api/distance**
Accepts `HTTP GET` requests.  
Returns all possible paths between two cities. Path information includes total distance and all the intermediate cities.
Query string should contain two required case-insensitive parameters of type `string`, representing city names: 
1. `from`
2. `to`

It will return HTTP status code `404 Not Found` if there is no path between requested cities or any of the requested cities is not found. Response body will contain `DistanceDTO` model with only `message` field set, detailing the error.  
Example URL:
`https://example.com/api/node/distance?from=Athens&to=Seattle`

### **/api/distance/byId**
Accepts `HTTP GET` requests.  
Returns all possible paths between two cities. Path information includes total distance and all the intermediate cities.  
Query string should contain two required parameters of type `long`, containing ids of the cities already present in the database: 
1. `from`
2. `to`

It will return HTTP status code `404 Not Found` if there is no path between requested cities or any of the requested cities is not found. Response body will contain `DistanceDTO` model with only `message` field set, detailing the error.  
Example URL:
`https://example.com/api/node/distance/byId?from=7&to=3`
